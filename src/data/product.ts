import type { ProductInterface } from "@/interfaces";

export default [
  {
    id: 1,
    image: "src/assets/images/macbook.jpg",
    title: "Macbook Pro",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 1500,
    category: "desktop",
  },
  {
    id: 2,
    image: "src/assets/images/levono.jpg",
    title: "Levono Pro",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 2300,
    category: "desktop",
  },
  {
    id: 3,
    image: "src/assets/images/rider.jpg",
    title: "Rider",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 1200,
    category: "gamer",
  },
  {
    id: 4,
    image: "src/assets/images/ldlc.jpg",
    title: "LDLC benolo",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 4500,
    category: "streaming",
  },
  {
    id: 5,
    image: "src/assets/images/asus.jpg",
    title: "Asus gamer",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 3755,
    category: "gamer",
  },
  {
    id: 6,
    image: "src/assets/images/rog.jpg",
    title: "Rog desktop",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 2452,
    category: "streaming",
  },
  {
    id: 7,
    image: "src/assets/images/msi.jpg",
    title: "MSI play",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 1478,
    category: "gamer",
  },
  {
    id: 8,
    image: "src/assets/images/pad.webp",
    title: "Think pad",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing edolor tempore ipsam cum ipsum reiciendis",
    price: 899,
    category: "desktop",
  },
] as ProductInterface[];

import type { ProductInterface } from "./Product.interface";

export interface CartProductInterface extends ProductInterface {
  qty: number;
}
